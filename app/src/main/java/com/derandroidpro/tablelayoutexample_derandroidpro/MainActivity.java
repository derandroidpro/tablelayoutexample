package com.derandroidpro.tablelayoutexample_derandroidpro;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TableLayout table;

    // in dieser ArrayLists werden die ArrayLists der Zeilen gespeichert, welche wiederum die Layouts der einzelnen Zellen innerhalb einer Zeile enthalten.
    ArrayList<ArrayList<LinearLayout>> cellLayouts = new ArrayList<>();

    // Anzahl Zeilen und Spalten (Kann natürlich auch beim erstellen der Tabelle dynamisch im Code festgelegt werden):
    int rows = 100;
    int columns = 7;

    // Konstanten für das Layout
    int colorEvenRow = Color.parseColor("#00000000"); // transparent
    int colorUnevenRow = Color.parseColor("#333F51B5");
    int imageWidth = 100;
    int imageHeight = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        table = findViewById(R.id.tableLayout);
        setupTable(rows, columns);

        fillCellsWithDummyText();
    }

    private void setupTable(int rows, int columns){
        // für jede Zeile in der Tabelle werden die Layouts erzeugt:
        for(int r = 0; r < rows; r++){
            // Erstellen und einrichten der TableRow für die aktuelle Zeile:
            ArrayList<LinearLayout> cellsOfRow = new ArrayList<>();
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

            // Vertikale Trennstriche für die aktuelle Reihe hinzufügen:
            tableRow.setDividerDrawable(getResources().getDrawable(R.drawable.table_divider1));
            tableRow.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
            tableRow.setVerticalGravity(Gravity.CENTER_VERTICAL);

            // Wenn die Zeile eine gerade Nummer hat, bekommt sie einen weißen Hintergrund, ansonsten einen blauen (die Nummerierung fängt bei 0 an):
            if(r % 2 == 0){
                tableRow.setBackgroundColor(colorEvenRow);
            } else {
                tableRow.setBackgroundColor(colorUnevenRow);
            }

            // Für jede Spalte der aktuellen Zeile wird ein LinearLayout erzeugt und der aktuellen TableRow hinzugefügt:
            for(int c = 0; c < columns; c++){
                LinearLayout cellLayout = new LinearLayout(this);
                cellLayout.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                cellLayout.setOrientation(LinearLayout.VERTICAL);
                cellLayout.setPadding(10, 5, 10, 5);
                cellLayout.setVerticalGravity(Gravity.CENTER_VERTICAL);

                cellsOfRow.add(cellLayout);
                tableRow.addView(cellLayout);
            }
            // der ArrayList, die alle Layouts aller Zeilen speichert, wird die ArrayList mit den Layouts der aktuellen Zeile hinzugefügt:
            cellLayouts.add(cellsOfRow);
            // die generierte Zeile wird in das TableLayout eingefügt:
            table.addView(tableRow);
        }
    }

    private void setCellText(int row, int column, String text, int imageRes, boolean isHeader){
        // TextView erzeugen und den übergebenen Text einsetzen:
        TextView tv = new TextView(this);
        tv.setText(text);
        tv.setTextColor(Color.BLACK);
        tv.setTextAppearance(this, android.R.style.TextAppearance_Medium);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        // Wenn die Zelle eine Überschriftszelle ist, wird der Text fett gemacht:
        if(isHeader){
            tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
        }

        // Du wolltest ja noch ein Icon neben dem Text haben. Dieses kann man ganz einfach in das TextView mit einfügen, ohne ein ImageView zu verwenden:
        // Wenn du einem Text einer Zelle KEIN Icon geben möchtest, übergibst du dieser Methode setCellText() als imageRes einfach eine 0.
        if(imageRes != 0) {
            Drawable iconDrawable = getDrawable(imageRes);
            // Höhe des Icons auf die Höhe einer Textzeile anpassen:
            iconDrawable.setBounds(0 ,0 ,tv.getLineHeight(), tv.getLineHeight());
            tv.setCompoundDrawables(iconDrawable, null, null, null);
        }
        // Das TextView wird dem LinearLayout der gewählten Zelle hinzugefügt:
        LinearLayout cellLayout = cellLayouts.get(row).get(column);
        cellLayout.removeAllViews(); // <- bestehende Elemente aus dem Layout entfernen (insofern vorhanden) um Dopplungen zu verhindern
        cellLayout.addView(tv);
    }

    private void setCellImage(int row, int column, int imageRes){
        // ImageView erzeugen und die übergebene Bildressource einsetzen:
        ImageView iv = new ImageView(this);
        LinearLayout.LayoutParams ivLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ivLayoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        iv.setLayoutParams(ivLayoutParams);
        iv.getLayoutParams().width = imageWidth;
        iv.getLayoutParams().height = imageHeight;
        iv.setImageResource(imageRes);

        // Das ImageView wird dem LinearLayout der gewählten Zelle hinzugefügt:
        LinearLayout cellLayout = cellLayouts.get(row).get(column);
        cellLayout.removeAllViews(); // <- bestehende Elemente aus dem Layout entfernen (insofern vorhanden) um Dopplungen zu verhindern
        cellLayout.addView(iv);
    }

    // Methode, die alle Zellen mit Platzhalterinhalten füllt (nur zu Demonstrationszwecken):
    private void fillCellsWithDummyText(){
        for(int r = 0; r < rows; r++){
            for(int c = 0; c < columns; c++){
                if(r == 0){
                    // Wenn es eine Zelle in der ersten Zeile ist: Überschrift einfügen
                    // Daher, dass hier kein Icon eingefügt werden soll, übergeben wir hier als imageRes eine 0, um der Methode zu signalisieren, dass kein Icon gesetzt werden soll:
                    setCellText(r, c, "Spalte " + Integer.toString(c + 1), 0,  true);
                } else if (c == 0){
                    // Wenn es eine Zelle in der ersten Spalte ist: Bild einfügen
                    setCellImage(r, c, R.mipmap.icon_smiling_face);
                } else{
                    // Bei allen anderen Zellen einfach die Zeilen- und Spaltennummer als Text einfügen:
                    setCellText(r, c, "Z: " + Integer.toString(r + 1) + ", S: " + Integer.toString(c + 1), R.mipmap.icon_camera, false);
                }
            }
        }
    }
}
